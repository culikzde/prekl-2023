from lexer import *

def readItem (inp) :
    name = inp.readIdentifier ();
    inp.checkSeparator (":")
    field_type = inp.readIdentifier ();
    if inp.isSeparator (";") :
       inp.nextToken () # skip ;

    # print ("FIELD", name, ":", field_type)
    return name, field_type


def readTable (inp) :
    inp.nextToken () # skip table
    table_name = inp.readIdentifier ();

    code = "class " + table_name + " (object) :" + "\n"
    code += "    def __init__ (self) :" + "\n"
    code += "       super (" + table_name + ", self).__init__ ()" + "\n"

    inp.checkSeparator ("{")

    while not inp.isSeparator ("}") :
        field_name, field_type = readItem (inp)
        init = "None"
        if field_type == "integer" :
            init = "0"
        elif field_type == "string" :
            init = '""'
        code += "       self." + field_name + " = " + init + "\n"

    inp.checkSeparator ("}")

    print (code)
    exec (code)
    t = Abc ()


inp = Lexer ()
inp.openFile ("input.txt")

while inp.token != inp.eos :
    if inp.isKeyword ("table") :
        readTable (inp)
    else :
        print (inp.tokenText)
        inp.nextToken ()
