#!/usr/bin/env python

from __future__ import print_function

import os, sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

import clang.cindex
# from clang.cindex import Index, Config, CursorKind
# dnf install python3-clang

# --------------------------------------------------------------------------

class TreeItem (QTreeWidgetItem):
    def __init__ (self, parent, text):
        QTreeWidgetItem.__init__ (self, parent)
        self.setText (0, text)
        self.fileName = ""
        self.line = 0
        self.column = 0

class TreeView (QTreeWidget):
    def __init__ (self, parent=None):
        QTreeWidget.__init__ (self, parent)
        self.setColumnCount (1)
        self.header ().hide ()
        self.setEditTriggers (QAbstractItemView.SelectedClicked)
        self.setIndentation (8);

# --------------------------------------------------------------------------

class MainWin (QMainWindow):

    def __init__ (self, parent=None):
        QMainWindow.__init__ (self, parent)

        self.setStatusBar (QStatusBar ())

        self.split = QSplitter ()
        self.setCentralWidget (self.split)

        self.tree = TreeView ()
        self.split.addWidget (self.tree)

        self.editTabs = QTabWidget ()
        self.split.addWidget (self.editTabs)

        self.setupMenus ()

        self.editors = { }
        self.translation_unit = None

        self.tree.itemClicked.connect (self.tree_clicked)



    def setupMenus (self):

        fileMenu = self.menuBar().addMenu ("&File")

        act = QAction ("&Quit", self)
        act.setShortcut ("Ctrl+Q")
        act.triggered.connect (self.close)
        fileMenu.addAction (act)

        editMenu = self.menuBar().addMenu ("&Edit")

        act = QAction ("Complete", self)
        act.setShortcut ("F9")
        act.triggered.connect (self.complete)
        editMenu.addAction (act)

    def tree_clicked (self, node) :
        txt = node.fileName + " line: " + str (node.line) + " col: " + str (node.column)
        self.statusBar().showMessage (txt)
        self.openSource (node.fileName, node.line, node.column)

    def openSource (self, fileName, line = 0, col = 0) :

        if fileName not in self.editors :

           view = QTextEdit ()
           view.setLineWrapMode (QTextEdit.NoWrap)

           name = os.path.basename (fileName)
           self.editTabs.addTab (view, name)
           self.editors [fileName] = view

           try :
              text = open(fileName).read()
              view.setText (text)
           except :
              pass

        edit = self.editors[fileName]
        self.editTabs.setCurrentWidget (edit)

        cursor = edit.textCursor () # cursor from QTextEdit

        cursor.movePosition (QTextCursor.Start)
        cursor.movePosition (QTextCursor.Down, QTextCursor.MoveAnchor, line-1)
        # cursor.movePosition (QTextCursor.Right, QTextCursor.MoveAnchor, col-1)
        cursor.movePosition (QTextCursor.Down, QTextCursor.KeepAnchor, 1)

        edit.setTextCursor (cursor)

    def complete (self):
        if self.translation_unit != None :

           fileName = ""
           edit = self.editTabs.currentWidget ()
           for name in self.editors :
               if self.editors [name] == edit :
                  fileName = name

           cursor = edit.textCursor ()
           line = cursor.blockNumber () + 1
           column = cursor.positionInBlock () + 1

           # print (fileName, str (line), str (column))

           answer = self.translation_unit.codeComplete (fileName, line, column,
                                                        unsaved_files=None,
                                                        include_macros=False,
                                                        include_code_patterns=False,
                                                        include_brief_comments=False)
           if answer != None :
              menu = QMenu (self);
              for t in answer.results :
                 print (t)
                 for u in t.string :
                   if u.isKindTypedText () :
                      action = QAction (u.spelling, menu)
                      menu.addAction (action)
              menu.exec_ (QCursor.pos())

    def display (self, above, item) :
        txt =  (
                 str (item.displayname) + " " +
                 str (item.kind).replace ("CursorKind.", "")
                 # str (item.location)
                )
        node = TreeItem (above, txt)

        loc = item.location
        node.fileName = str (loc.file)
        node.line = loc.line
        node.column = loc.column

        for t in item.get_children () :
            self.display (node, t)

    def translate (self, fileName) :
        self.openSource (fileName)
        index = clang.cindex.Index.create ()
        tu = index.parse (fileName)
        self.translation_unit = tu
        if tu :
           for t in tu.diagnostics :
              print (t)
           self.display (self.tree, tu.cursor)
           self.tree.expandAll ()

# --------------------------------------------------------------------------

app = QApplication (sys.argv)
win = MainWin ()
win.show ()
win.translate ("sample.cc")
app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

